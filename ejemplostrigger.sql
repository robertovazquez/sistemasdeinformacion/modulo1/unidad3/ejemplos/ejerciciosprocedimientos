﻿CREATE DATABASE IF NOT EXISTS ejemplostrigger;
USE ejemplostrigger;


CREATE OR REPLACE TABLE local (

    id int AUTO_INCREMENT,
    nombre varchar (20),
    precio float,
    plazas int,
 PRIMARY KEY(id)
);



CREATE OR REPLACE TABLE usuario (

 id int AUTO_INCREMENT,
  nombre varchar(20),
  incial char (1),
  PRIMARY KEY (id)
);



CREATE OR REPLACE TABLE usa (

   local int,
   usuario int,
   dias int,
   fecha date,
   total float,

   PRIMARY KEY (local,usuario)




);


-- quiero que cuando introduzcas un usuario me calcuule y almacene la inicial de su nombre




DELIMITER //

CREATE OR REPLACE TRIGGER usuarioBI
  BEFORE INSERT ON usuario
  FOR EACH ROW
  BEGIN
 
   SET new.incial=LEFT(new.nombre,1);

END //
DELIMITER;

INSERT INTO usuario (id, nombre, incial)
  VALUES (0, 'Roberto', 'R');
SELECT * FROM usuario u;