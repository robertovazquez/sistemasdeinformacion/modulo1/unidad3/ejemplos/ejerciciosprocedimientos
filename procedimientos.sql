﻿
DROP DATABASE IF EXISTS programacion;

CREATE DATABASE programacion;

USE programacion;


-- ejemplos de procedimientos almacenados


  -- p1 crear un procedimento almacenado muestre la fecha de hoy

 DELIMITER //
 CREATE OR REPLACE PROCEDURE p1()
    BEGIN 
    SELECT NOW();
    END //
DELIMITER;


CALL p1();


-- crear una variable de tipo datetime y almacenar en esa varibale la fecha de hoy y nos muestre la variable

 DELIMITER //
 CREATE OR REPLACE PROCEDURE p2()
 BEGIN
    -- declar
   DECLARE v datetime;
   -- input (inicializar variable)
   SET v=NOW();
  -- output
   SELECT v;
END //
DELIMITER;


 CALL p2();



 -- procedimiento suma de dos numeros que recibe como argumentos dos numeros y me muestra la suma


 DELIMITER //
 CREATE OR REPLACE PROCEDURE p3(num1 int,num2 int)
 BEGIN
 DECLARE suma int DEFAULT 0;
  SET suma= num1+num2;
  SELECT suma;

END //
DELIMITER;

CALL p3(1,5);

CALL p3 (45,67);


-- restar suma de dos numeros


 DELIMITER //
 CREATE OR REPLACE PROCEDURE p4 (num1 int, num2 int)
 BEGIN
  DECLARE resultado int DEFAULT 0;
  SET resultado=num1-num2;
  SELECT resultado;
 
END //
DELIMITER;

CALL p4(5,4);


-- crea un procedimiento que sume dos numeros y los almacene en una tabla llamada datos. la tabla la debe crear en caso de que no exista.

   DELIMITER //
   CREATE OR REPLACE PROCEDURE p5(num1 int,num2 int)
   BEGIN

 -- declarar e inicializar las variables
     DECLARE resultado int DEFAULT 0;
-- crear tabla
    CREATE TABLE  IF NOT EXISTS datos (
        id int AUTO_INCREMENT,
        d1 int DEFAULT 0,
        d2 int DEFAULT 0,
        suma int DEFAULT 0,
       PRIMARY KEY (id)
    );
   
   -- realizar los calculos

    SET resultado=num1+num2;


-- almacenar datos en la tabla

     INSERT INTO datos VALUES(DEFAULT, num1,num2,resultado);

  
  END //
  DELIMITER;

CALL p5(14,6);
CALL p5(34,54);


SELECT * FROM datos d;


-- suma producto 2 numeros 
  -- crear una tabla llamada sumaproducto(id,d1,d2,suma,producto)
  -- introducir los datos en la tabla y los resultados

 DELIMITER //
 CREATE OR REPLACE PROCEDURE p6(num1 int,num2 int)
  
 BEGIN
 DECLARE suma int DEFAULT 0;
  DECLARE producto int DEFAULT 0;
  CREATE TABLE IF NOT EXISTS sumaproductos (
        id int AUTO_INCREMENT,
        d1 int DEFAULT 0,
        d2 int DEFAULT 0,
        suma int DEFAULT 0,
        producto int DEFAULT 0,
       PRIMARY KEY (id)
  
   );

   SET suma=num1+num2;
   SET producto=num1*num2;

  INSERT INTO sumaproductos (id,d1,d2,suma,producto)
  VALUE  (DEFAULT,num1,num2,suma,producto);


END //
DELIMITER;


CALL p6(3,5);

SELECT * FROM sumaproductos s;


 DELIMITER //
 CREATE OR REPLACE PROCEDURE nombre(p1 int)
 BEGIN
 

END //
DELIMITER;

-- p7 objetivo elevar un numero a otro.Almacenar el resurtado y los numeros en la tabla potencia; base y exponente

DELIMITER //
CREATE OR REPLACE PROCEDURE p7 ( base int, exponente int)

  BEGIN
    
      DECLARE resultado int DEFAULT 0;
    SET resultado=POW(base,exponente); 

   CREATE TABLE IF NOT EXISTS potencia(
      id int AUTO_INCREMENT,
      b int,
      e int,
      r int,
      PRIMARY KEY (id)
     
  );

 INSERT INTO potencia (b,e,r) 
 VALUES (base,exponente,resultado);
   


  END//
  DELIMITER;


CALL p7 (2,3);
SELECT * FROM potencia p;

-- p8 objetivo: realizar la raiz cuadrada de un numero, Almacenar el numero y su raiz en la tabla raiz

 DELIMITER //
 CREATE OR REPLACE PROCEDURE p8 (num1 int)
 BEGIN
   DECLARE vraiz float DEFAULT 0;
   SET vraiz=SQRT(num1);
   CREATE TABLE IF NOT EXISTS raiz (
      id int AUTO_INCREMENT,
      numero1 int,
      resultado float,
    PRIMARY KEY (id)
  
  );

   INSERT INTO raiz (numero1,resultado) VALUES (num1,vraiz);
END //
DELIMITER;

CALL p8 (9);

SELECT * FROM  raiz r;

-- analizar siguiente procedimiento y añadir lo necesario para que la longitud del texto y lo almacene en la tabla en un campo llamado longitud


   DELIMITER //
  
   DROP PROCEDURE IF EXISTS p9 //
   CREATE OR REPLACE PROCEDURE p9(argumento varchar (50))
   BEGIN
      DECLARE  vlongitud int DEFAULT 0;
      SET vlongitud=CHAR_LENGTH(argumento);
   CREATE TABLE IF NOT EXISTS texto (
     id int AUTO_INCREMENT PRIMARY KEY,
     texto varchar (50),
      longitud int
    );


   insert  INTO texto(texto,longitud) VALUE (argumento,vlongitud);
  
  END //
  DELIMITER;

 CALL p9("santander");

 SELECT * FROM texto;


-- sintaxis basica del if

 /**
   IF (condicion)THEN
    sentencia verdadero;
    ELSE
    sentencia falso;
    END IF;

    **/

  -- p10 procedimento al cual le paso un numero y me indica si es mayor

   DELIMITER  //



 CREATE OR REPLACE PROCEDURE p10 (numero int)

  BEGIN
    
      IF (numero>100) THEN
         SELECT "mayor que 100";
      END IF;



  END//

DELIMITER;

 CALL p10(120);
 CALL p10(90);


-- p11 procediemiento al cual le pasas un numero y me indicas si es mayor , igual o menor que 100

   DELIMITER //
   CREATE OR REPLACE PROCEDURE p11(numero int)

   BEGIN
      IF (numero>100) THEN
        SELECT "numero mayor que 100";
          ELSEIF (numero<100) THEN
        SELECT "numero menor que 100";
          Else 
          SELECT "numero igual a 100";
        
       END IF;       
         
 
 
 
 END //
  DELIMITER;

CALL p11(123);
CALL p11(90);
CALL p11(100);

-- mejor programado aunque igual


 DELIMITER //
   CREATE OR REPLACE PROCEDURE p11modificado(numero int)

   BEGIN
    DECLARE resultado varchar (50)DEFAULT "igual a 100";
      IF (numero>100) THEN
        SET resultado="numero mayor que 100";
          ELSEIF (numero<100) THEN
         SET resultado="numero menor que 100";
         
            END IF;       
         
    SELECT resultado;
 
 
 END //
  DELIMITER;

 CALL p11modificado(123);
CALL p11modificado(90);
CALL p11modificado(100);


-- p12 
-- crear un procedimiento que le pasas una nota y te debe devolver lo siente
  -- suspenso si la nota es menor que 5
  -- suficiente si la nota esta entre 5 y 7
  -- notable si la nota esta entre 7 y 9
 -- sobresalientes si la nota es mayor que 9


  DELIMITER //
 CREATE OR REPLACE PROCEDURE p12 (nota int)

  BEGIN

      IF (nota<5) THEN
        SELECT "supenso como una catedral";
          ELSEIF (nota>=5 AND nota<7) THEN
        SELECT "aprobado";
          ELSEIF (nota>=7 AND nota<9) THEN
          SELECT "notable";
          ELSEIF (nota>=9) THEN
          SELECT "sobresaliente";
        
       END IF;       


  END //
DELIMITER;


 CALL p12(4);
CALL p12(6);
CALL p12(8);
CALL p12(9);

-- p12 con variable

DELIMITER //
   CREATE OR REPLACE PROCEDURE p12modificado(n int)

   BEGIN
    DECLARE r varchar (50)DEFAULT "suspenso";
      IF (n>=5 AND n<7) THEN
        SET r="Suficiente";
          ELSEIF (n>=7 AND n<9) THEN
         SET r="notable";
           ELSEIF (n>=9) THEN
         SET r="sobresaliente";
            END IF;       
         
    SELECT r;
 
 
 END //
  DELIMITER;
CALL p12modificado(4);
CALL p12modificado(6);
CALL p12modificado(8);
CALL p12modificado(9);



-- p13 ejercicio anterior con case



DELIMITER //
   CREATE OR REPLACE PROCEDURE p13(n int)

   BEGIN

    DECLARE r varchar (50);
     
    CASE 
      WHEN (n>=5 AND n<7) THEN
       SET r="Suficiente";
        WHEN (n>=7 AND n<9) THEN
        SET r="notable";
         WHEN (n>=9) THEN
         SET r="sobresaliente";
        ELSE
         SET r="suspenso";
      END CASE;       
        
    SELECT r;

 END //
 DELIMITER;

CALL p13(4);
CALL p13(6);
CALL p13(8);
CALL p13(9);


-- argumentos de Entrada / Salida

/**
 p14

Quiero calcular la suma de dos numeros. 
La suma la debe almacenar en un argumento de salida llamado resultado
  call p14 (n1 int,n2 int,out resultado int)


  
  **/

 DELIMITER //
 CREATE OR REPLACE PROCEDURE p14(n1 int,n2 int,out resultado int)

 BEGIN
 SET resultado=n1+n2;

END //
DELIMITER;

SET @suma=0;
CALL p14(2,5,@suma);
SELECT @suma;


/** p15
  procedimento que recibe como argumentos;
  un numero n1 (argumento de entrada)
  otro numero n2 (argumento de entrada)
  resultado numero (argumento de salida o entrada/salida)
  calculo  texto (argumento de entrada)
  si calculo vale suma entonces resultado tendra la suman de n1+n2
  si calculo vale productos entonces resultado tenda el producto de n1 *n2
  **/



 DELIMITER //
 CREATE OR REPLACE PROCEDURE p15(n1 int,n2 int,OUT resultado int,calculo varchar(50))
 BEGIN
  
    IF (calculo='suma') THEN
      set resultado=n1+n2;
      ELSEIF (calculo='producto') THEN
       SET  resultado=n1*n2;
      END IF;

END //
DELIMITER;


CALL p15(4,5,@r,"producto");
SELECT @r;


/** p16
  utilizando while crear un procedimento que muestre en pantalla numeros de el 1 al 10 **/



 DELIMITER //
 DROP PROCEDURE IF EXISTS p16;
 CREATE OR REPLACE PROCEDURE p16()
 BEGIN
 DECLARE contador int DEFAULT 1;

     WHILE (contador<11) DO
       SELECT contador;
      SET contador=contador +1;
      END WHILE;





END //
DELIMITER;

CALL p16();

-- insertar los numeros en una tabla del anterior ejercicio

 DELIMITER //
 DROP PROCEDURE IF EXISTS p17;
 CREATE OR REPLACE PROCEDURE p17()

 BEGIN
 DECLARE contador int DEFAULT 1;
  CREATE OR REPLACE TEMPORARY TABLE datos(
  
num int
 );
 
 WHILE (contador<=10) DO
       INSERT INTO datos VALUES (contador);
       SET contador=contador +1;
 END WHILE;

  SELECT * FROM datos d;

END //
DELIMITER;

CALL p17();

/**
  p18
  Crear un procedimento que le pases un numero y te muestre desde el numero 1 hasta el numero pasado
  Utilizar tabla temporal.Realizarlo con while
**/
  

 DELIMITER //
 DROP PROCEDURE IF EXISTS p18;
 CREATE OR REPLACE PROCEDURE p18(numero int)
 BEGIN
  DECLARE contador int DEFAULT 1;
  CREATE OR REPLACE TEMPORARY TABLE datos (
   num int
  
  );

    WHILE (contador<=numero) DO
      INSERT INTO datos VALUES (contador);
      SET contador=contador +1;
      END WHILE;

       SELECT * FROM datos;

      
 

END //
DELIMITER;

CALL p18(2);



  -- f1 le paso 


  DELIMITER //
 CREATE OR REPLACE FUNCTION f1 (n1 int,n2 int)
  RETURNS int
  BEGIN
    DECLARE resultado int DEFAULT 0;   

     SET resultado=n1+n2;
    RETURN resultado;
  END //
  DELIMITER;

SELECT f1 (5,6);


