﻿DROP DATABASE IF EXISTS ejemploprogramacion;

CREATE DATABASE ejemploprogramacion;

USE ejemploprogramacion;

-- ejercicio 1

  -- con if 

 DELIMITER //

 DROP PROCEDURE IF EXISTS nombre;
 CREATE OR REPLACE PROCEDURE nombre (num1 int,num2 int)

 BEGIN
  IF num1>num2 THEN
      SELECT num1;
     ELSE
      SELECT num2;
  END IF;

END //
DELIMITER;

CALL nombre (12,5);


-- con totales


 DELIMITER //
 DROP PROCEDURE IF EXISTS ej1v2;
 CREATE OR REPLACE PROCEDURE ej1v2(a int,b int)
 BEGIN
  CREATE TEMPORARY TABLE IF NOT EXISTS numeros(
   numero int
  );
  INSERT INTO numeros (numero)VALUES (a),(b);
  -- SELECT * FROM numeros;
  SELECT MAX(numero) FROM numeros;
  DROP TABLE numeros;

END //
DELIMITER;
 CALL ej1v2(12,55);

-- funcion mysqul

 DELIMITER //
DROP PROCEDURE IF EXISTS ej1v3;
 CREATE OR REPLACE PROCEDURE ej1v3(num1 int, num2 int)
 BEGIN
  DECLARE numeros int DEFAULT 0;
  SET numeros=GREATEST(num1,num2);
  SELECT numeros;
 

END //
DELIMITER;

CALL ej1v3(12,55);



-- ejercicio 2

-- con if


 DELIMITER //

 DROP PROCEDURE IF EXISTS ej2v1;
 CREATE OR REPLACE PROCEDURE ej2v1 (num1 int,num2 int,num3 int)

 BEGIN
   DECLARE s int DEFAULT num3;
  IF (num1>num2) THEN
       IF (num1>num3) THEN
        SET s=num1;
        END IF;
        ELSEIF (num2>num3) THEN
        SET s=num2;
        END IF;
        SET s=num3;
        
   --  SELECT s;

END //
DELIMITER;
 CALL ej2v1(12,55,44);

 
 -- con totales


 DELIMITER //
 DROP PROCEDURE IF EXISTS ej2v2;
 CREATE OR REPLACE PROCEDURE ej2v2(a int,b int, c int)
 BEGIN
  CREATE TEMPORARY TABLE IF NOT EXISTS numeros(
   numero int
  );
  INSERT INTO numeros (numero)VALUES (a),(b),(c);
  -- SELECT * FROM numeros;
  SELECT MAX(numero) FROM numeros;
  DROP TABLE numeros;

END //
DELIMITER;
CALL ej2v2(12,55,44);



  -- con funcion

 DELIMITER //
DROP PROCEDURE IF EXISTS ej2v3;
 CREATE OR REPLACE PROCEDURE ej2v3(num1 int, num2 int, num3 int)
 BEGIN
  DECLARE numeros int DEFAULT 0;
  SET numeros=GREATEST(num1,num2,num3);
  SELECT numeros;
 

END //
DELIMITER;

CALL ej2v3(12,55,44);

-- ejercicio 3

   DELIMITER //
  DROP PROCEDURE IF EXISTS ej3;
   CREATE OR REPLACE PROCEDURE ej3(n1 int, n2 int,n3 int,OUT pequeño int, OUT grande int)
   BEGIN
       SET grande=GREATEST(n1,n2,n3);
       SeT pequeño=LEAST(n1,n2,n3);
  
  END //
  DELIMITER;

 CALL ej3(4,6,8,@p,@g);
 SELECT @p,@g;

  
-- ejercicio4

 DELIMITER //
 DROP PROCEDURE IF EXISTS ej4;
 CREATE OR REPLACE PROCEDURE ej4(fecha1 date, fecha2 date,OUT dias int)
 BEGIN
    
   SET dias= DATEDIFF(fecha1,fecha2);

END //
DELIMITER;

CALL ej4 ('2010/2/3','2010/7/10',@f);
SELECT @f;

-- ejercicio 5

 DELIMITER //
 DROP PROCEDURE IF EXISTS ej5;
 CREATE OR REPLACE PROCEDURE ej5(fe1 date, fe2 date,OUT mes int)
 BEGIN
    
   SET mes=TIMESTAMPDIFF (MONTH,fe1,fe2);

END //
DELIMITER;


CALL ej5 ('2010/2/3','2010/7/10',@f);
SELECT @f;


-- ejercicio 6

 DELIMITER //
 DROP PROCEDURE IF EXISTS ej6;
 CREATE OR REPLACE PROCEDURE ej6(fe1 date,fe2 date,OUT dia int, OUT mes int,OUT ano int)
 BEGIN
  SET dia= DATEDIFF(fe1,fe2);
  SET mes=TIMESTAMPDIFF (MONTH,fe1,fe2);
  SET ano=TIMESTAMPDIFF (YEAR,fe1,fe2);

END //
DELIMITER;
CALL ej6 ('2010/2/3','2011/7/10',@d,@m,@a);
SELECT @d,@m,@a;

-- ejercicio7

 DELIMITER //
 DROP PROCEDURE IF EXISTS ej7;
 CREATE OR REPLACE PROCEDURE ej7(frase varchar(50),OUT contar int)
 BEGIN
   SET contar=CHAR_LENGTH(frase);
END //
DELIMITER;

CALL ej7('ejemplodeejerccio',@f);
SELECT @f;

