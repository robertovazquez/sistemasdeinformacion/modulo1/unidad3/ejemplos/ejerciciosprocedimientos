﻿DROP DATABASE IF EXISTS ejemplo2programacion;

CREATE DATABASE ejemplo2programacion;

USE ejemplo2programacion;


-- ejercicio 1

   DELIMITER //

   DROP PROCEDURE IF EXISTS ej1ejem2;
   CREATE OR REPLACE PROCEDURE ej1ejem2( IN texto varchar(50), IN caracter char(1))
   BEGIN
    DECLARE resultado varchar (50) DEFAULT "no esta";
    IF ( LOCATE(caracter,texto)<>0) THEN
      SET resultado="esta";
      END IF;
       select resultado;
   END //
  DELIMITER;

CALL ej1ejem2('texto comprobacion',@r );

 -- ejercicio 2

   DELIMITER //
   DROP PROCEDURE IF EXISTS ej2ejem2;
   CREATE OR REPLACE PROCEDURE ej2ejem2 ( IN texto varchar(50), IN caracter char(1))
   BEGIN
     DECLARE resultado varchar(50) DEFAULT 0;
      SET resultado=SUBSTRING_INDEX(texto,caracter,1);
     SELECT resultado;


  
  END //
  DELIMITER;
 CALL ej2ejem2('texto comprobacion',@r);


-- ejercicio 3

 DELIMITER //
 DROP PROCEDURE IF EXISTS ej3ejemp2;
 CREATE OR REPLACE PROCEDURE eje3ejem2(num1 int,num2 int,num3 int, OUT grandre int,OUT pequeño int)
 BEGIN
     
 SET grandre=IF(num1>num2,IF (num1>num3,num1,num2),if (num2>num3,num2,num3));
 SET pequeño=IF(num1<num2,IF (num1<num3,num1,num2),if (num2<num3,num2,num3));

END //
DELIMITER;

CALL eje3ejem2 (1,8,10,@g,@p);
SELECT @g,@p;

-- ejercicio 4

  CREATE TABLE datos (
  
   datos_id int(11) NOT NULL AUTO_INCREMENT,
   numero1 int (11) NOT NULL,
   numero2 int (11) NOT NULL,
   suma varchar(255) DEFAULT NULL,
   resta varchar(255)DEFAULT NULL,
   rango varchar(5)DEFAULT NULL,
   texto1 varchar(25)DEFAULT NULL,
   texto2 varchar(25)DEFAULT NULL,
   junto varchar(255)DEFAULT NULL,
   longuitud varchar(255)DEFAULT NULL,
   tipo int (11) NOT null,
   numero int (11) NOT NULL,
    PRIMARY KEY (datos_id)
  );


 DELIMITER //
 CREATE OR REPLACE PROCEDURE ej4ejem2()
 BEGIN
 DECLARE resultado int;
  DECLARE num1 int;
  DECLARE num2 int;

 SELECT COUNT(*) INTO num1 FROM datos d WHERE d.numero1>50;
 SET num2=( SELECT COUNT(*) FROM datos d WHERE d.numero2>50);
 sET resultado=num1+num2;
  SELECT resultado,num1,num2;
 

END //
DELIMITER;

CALL ej4ejem2();


-- ejercicio 5

 DELIMITER //

 CREATE OR REPLACE PROCEDURE ej5ejemp2()
 BEGIN
   UPDATE datos d
    SET d.suma=d.numero1+d.numero2;
   UPDATE datos d
    SET d.resta=d.numero1-d.numero2;
     
     

END //
DELIMITER;

-- ejercicio 6

 DELIMITER //
 DROP PROCEDURE IF EXISTS ej6ejemp2;
 CREATE OR REPLACE PROCEDURE ej6ejemp2()
 BEGIN
  UPDATE datos d
  set suma=NULL, d.resta=NULL;

   UPDATE datos d
     SET d.suma=d.numero1+d.numero2 WHERE d.numero1>d.numero2;
  UPDATE datos d
     SET d.resta=d.numero1-d.numero2 WHERE d.numero2>=d.numero1;
    
                                  


END //
DELIMITER;

-- ejercicio 7

 DELIMITER //

 CREATE OR REPLACE PROCEDURE ej7ejemp2()
 BEGIN
  UPDATE datos d
  SET d.junto=CONCAT(d.texto1," ",d.texto2);

END //
DELIMITER;




-- ejercicio 8



   DELIMITER //
 
   CREATE OR REPLACE PROCEDURE ej8ejemp2 ()
   BEGIN
   UPDATE datos d
   SET d.junto=NULL;

    UPDATE datos d
    SET d.junto=IF (d.rango='A',CONCAT(d.texto1," - ",d.texto2),IF(d.rango='B',CONCAT(d.texto1," + ",d.texto2),d.texto1));

    /*UPDATE datos d
    SET d.junto=CONCAT(d.texto1," - ",d.texto2)
    WHERE d.rango='A';
    UPDATE datos d
    SET d.junto=CONCAT(d.texto1," + ",d.texto2)
    WHERE d.rango='B';*/



  
  END //
  DELIMITER;

CALL ej8ejemp2();
SELECT * FROM datos d;








