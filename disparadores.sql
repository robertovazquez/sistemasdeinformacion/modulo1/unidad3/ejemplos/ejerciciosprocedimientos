﻿tbUSE programacion;

-- disparadores

  CREATE OR REPLACE TABLE salas (
  
    id int AUTO_INCREMENT PRIMARY KEY,
    butacas int,
    fecha date,
    edad int DEFAULT 0
  
  );

 CREATE OR REPLACE TABLE ventas (
  
    id int AUTO_INCREMENT PRIMARY KEY,
    sala int,
    numero int DEFAULT 0
  
  );


-- salaBI

    -- Controla la insercion de registros nuevos
  -- disparador para que me calcule la edad de la sala en funcion de su fecha de alta

INSERT INTO salas (butacas,salas)
VALUES (50,"2010/1/1");


DELIMITER //

CREATE OR REPLACE TRIGGER salaBI
  BEFORE INSERT ON salas
  FOR EACH ROW
  BEGIN
 SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
  /*
  UPDATE salas s
  SET s.edad=TIMESTAMPDIFF(year,s.fecha,NOW());

  */
END //
DELIMITER;

-- introduzco registro en la tabla para probar mi disparador

INSERT INTO salas (butacas,fecha)
VALUES (50,"2000/1/1");
SELECT * FROM salas;


-- salaBU

    -- Controla la actualizacion de registros nuevos
  -- disparador para que me calcule la edad de la sala en funcion de su fecha de alta


DELIMITER //

CREATE OR REPLACE TRIGGER salaBU
     
  BEFORE UPDATE ON salas
  FOR EACH ROW
  BEGIN

   SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW()); 



  END //
DELIMITER;


UPDATE salas 
  SET fecha="2002,1/1"
  WHERE id=1;

SELECT * FROM salas;

   -- vamos a comprobar el funcionamiento de los dos disparadores

  
  -- insertar

  INSERT INTO salas (butacas,fecha)
      VALUES (56,"2005/4/8"),(23,"1989/5/5");

     SELECT * FROM salas s;

-- actualizar 

    UPDATE salas 
      set fecha="1990/1/1";
     SELECT * FROM salas s;



  -- necesito que la tabla salas disponga de tres campos nuevos .

    -- esos campos seran dia , mes , año ,


    -- quiero que esos tres campos automaticamente tengan el dia, mes y año de la fecha


    -- necesito un disparador para insertar y otro para actualizar


    ALTER TABLE salas 
    
    ADD COLUMN dia int,
    ADD COLUMN mes int,
    ADD COLUMN anio int;
    



DELIMITER //

CREATE OR REPLACE TRIGGER salaBI
  BEFORE INSERT ON salas
  FOR EACH ROW
  BEGIN
   SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW());
   SET new.dia=DAY(new.fecha);
   SET new.mes=MONTH(new.fecha);
   SET new.anio=year (new.fecha);

END //
DELIMITER;



DELIMITER //

CREATE OR REPLACE TRIGGER salaBU
     
  BEFORE UPDATE ON salas
  FOR EACH ROW
  BEGIN

   SET new.edad=TIMESTAMPDIFF(year,new.fecha,NOW()); 
  SET new.dia=DAY(new.fecha);
   SET new.mes=MONTH(new.fecha);
  SET new.anio=year (new.fecha);

  END //
DELIMITER;

-- comprobar el funcionomientos 

  INSERT INTO salas (butacas,fecha)
      VALUES (51,"2002/4/8"),(21,"1981/5/5");

     SELECT * FROM salas s;


 UPDATE salas 
      set edad=10;
     SELECT * FROM salas s;



    -- crear tablas ventas1


      CREATE OR REPLACE TABLE ventas1 (
      
         id int,
         fecha date,
         preciounitario int,
         unidad int,
         preciofinal int
      
      
      );


   /** vamos a realizar un ejerccio que nos permita actualizar correctamente los datos en una nueva tabla
    ventas 1
     que deve calcular precio final con dispadador **/


DELIMITER //

CREATE OR REPLACE TRIGGER ventasBI
  BEFORE INSERT ON ventas1
  FOR EACH ROW
  BEGIN
 
   SET new.preciofinal=NEW.preciounitario*new.unidad;

END //
DELIMITER;


DELIMITER //

CREATE OR REPLACE TRIGGER ventasBU
  BEFORE UPDATE ON ventas1
  FOR EACH ROW
  BEGIN
 
   SET new.preciofinal=NEW.preciounitario*new.unidad;

END //
DELIMITER;





INSERT INTO ventas1 (fecha,preciounitario,unidad) VALUES ("2001/2/4",34,6), ("2001/5/7",32,5);

SELECT * FROM ventas1;



 UPDATE ventas1 v 
      set v.preciounitario=15;
     SELECT * FROM ventas1;






