﻿USE ejemplo6programacion ;



-- ejercicio 1

 DELIMITER //
CREATE OR REPLACE TRIGGER ventasBI
  BEFORE INSERT ON ventas
  FOR EACH ROW
  BEGIN

   SET new.total=new.unidades*new.precio;


  END //

DELIMITER;

SELECT * FROM ventas v;
SELECT * FROM productos p;



-- ejercicio 2


  DELIMITER //

CREATE OR REPLACE TRIGGER sumaAI
  AFTER INSERT ON ventas
  FOR EACH ROW
  BEGIN
   UPDATE productos p

    SET p.cantidad=p.cantidad + NEW.total WHERE p.producto=new.producto;
   


  END //
DELIMITER;

SELECT * FROM ventas v;
SELECT * FROM productos p;


-- ejercicio 3

 DELIMITER //
CREATE OR REPLACE TRIGGER ventas1BU
  BEFORE UPDATE ON ventas
  FOR EACH ROW
BEGIN

     SET new.total=new.unidades*new.precio;

     UPDATE productos p

      SET p.cantidad=p.cantidad+(new.total-OLD.total) WHERE producto=NEW.producto;
END //

DELIMITER;



-- ejercicio 4


  DELIMITER //

CREATE OR REPLACE TRIGGER sumaAU

  AFTER UPDATE ON  ventas
  FOR EACH ROW

  BEGIN

  UPDATE productos p

    SET p.cantidad=p.cantidad + NEW.total WHERE p.producto=new.producto;
    
      

  END //

DELIMITER;

SELECT * FROM  ventas v;
SELECT * FROM productos p;



-- ejercicio 5


  DELIMITER // 

CREATE OR REPLACE TRIGGER proBU

  BEFORE UPDATE ON productos
  FOR EACH ROW

BEGIN

     SET NEW.cantidad=(SELECT SUM(total) from ventas WHERE producto=NEW.producto);
 
 
 END //

DELIMITER; 

-- ejercicio 6 

  DELIMITER //

CREATE OR REPLACE TRIGGER eliBU

  AFTER UPDATE ON productos
  FOR EACH ROW
  BEGIN

     DELETE  FROM ventas WHERE producto=new.producto;

  END //

DELIMITER;

SELECT * FROM ventas v;
SELECT * FROM productos p;







 


   




  

