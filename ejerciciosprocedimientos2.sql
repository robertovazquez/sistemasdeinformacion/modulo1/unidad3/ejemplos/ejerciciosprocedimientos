﻿


USE ejemplo3programacion;

-- ejerccio 1

   -- tablas alumnos circulo conversion cuadrados grupos rectangulo y triangulos importados desde exel


  -- ejercicio2


 DELIMITER //
 
 CREATE OR REPLACE FUNCTION  ej2ejemp3(base int,altura int)
  RETURNS float
 BEGIN
 DECLARE area float;
  SET area=base*altura/2;
  RETURN area;


END //
DELIMITER;

-- ejercicio 3
 
DELIMITER //
CREATE OR REPLACE FUNCTION ej3ejemp3(base int,lado2 int,lado3 int)
  RETURNS float
  BEGIN 
DECLARE perimetro float;
  SET perimetro=base+lado2+lado3;
  RETURN perimetro;

  END //
DELIMITER;


-- ejercicio 4

DELIMITER //

CREATE OR REPLACE PROCEDURE ej4ejemp3( id int,id1 int,id2 int)

  BEGIN

   UPDATE triangulos t
     SET  t.area=areatriangulo (t.base,t.altura),
    t.perimetro=perimetrotriangulo (base,t.lado2,t.lado3)
  WHERE id BETWEEN id1 AND id2;

  END //

DELIMITER;

-- ejercicio 5

  DELIMITER //

CREATE OR REPLACE FUNCTION ej5ejemp3(lado int)
  RETURNS int (11)
  BEGIN
  DECLARE r float;
  
    SET r=POW(lado,2);
  RETURN r;
  
  END //

DELIMITER;

-- ejercicio 6

  DELIMITER //
CREATE OR REPLACE FUNCTION ej6ejemplo3(lado int)
  RETURNS int (11)
  BEGIN
  DECLARE p float;
   SET p=(lado*4);
  RETURN p;
   
  END //
DELIMITER

-- ejercicio 7

DELIMITER //

CREATE OR REPLACE PROCEDURE  ej7ejemp3(id1 int,id2 int)


  BEGIN
  UPDATE cuadrados c

   SET c.area=c.areaCuadrado (c.lado),c.perimetro=c.perimetroCuadrado(c.lado) BETWEEN id1 AND id2;

  END //

   DELIMITER;

-- ejercicio 8

    
  DELIMITER //

CREATE OR REPLACE FUNCTION ej8ejemp3(radio float)

  RETURNS float

  BEGIN 

       DECLARE r float;

          SET r=PI()*POW(radio,2);
      
          RETURN r;
     
      END //
DELIMITER;

-- ejercicio 9


  DELIMITER //

CREATE OR REPLACE FUNCTION ej9ejemp3(lado1 float,lado2 float)

     RETURNS float

  BEGIN
         DECLARE r  float;

        

      SET r=2*(lado1+lado2);
         
        RETURN r;
            
  
   END //

DELIMITER;

SELECT ej9ejemp3(2,5);


-- ejercicio 10



  DELIMITER //

CREATE OR REPLACE PROCEDURE ej10ejemp3(id1 int,id2 int)

  BEGIN
      
         
    UPDATE circulo c 
   SET c.area=areacirculo(c.radio),c.perimetro=perimetrocirculo(c.radio) WHERE c.id BETWEEN id1 AND id2
else

   SET c.area=areacirculo(c.radio),c.perimetro=perimetrocirculo(c.radio)  BETWEEN id1 AND id2;
   
   END //

DELIMITER;


-- ejercicio 11

DELIMITER //

CREATE OR REPLACE FUNCTION ej11ejmp3(n1 int,n2 int,n3 int,n4 int)
RETURNS float
  BEGIN
   DECLARE r float;
    SET r=(n1+n2+n3+n4)/4;
  RETURN r;

  END //
DELIMITER


SELECT ej11ejmp3(6,4,5,6);


-- ejercicio 12

  DELIMITER //

CREATE OR REPLACE FUNCTION ej12ejemp3(n1 int,n2 int, n3 int, n4 int)

     RETURNS int (11)

  BEGIN
      DECLARE m int;

  SET m=LEAST(n1,n2,n3,n4);


  RETURN m;
    END //

DELIMITER;


SELECT ej12ejemp3(3,6,7,8);


-- ejercicio 13

  DELIMITER //
  CREATE OR REPLACE FUNCTION ej13ejemp3(n1 int,n2 int, n3 int, n4 int)
  RETURNS int (11)
  BEGIN

    DECLARE m int;

    SET m=GREATEST(n1,n2,n3,n4);

    RETURN m;

  END   //

  DELIMITER;

SELECT ej13ejemp3(4,5,6,8);



-- ejercicio 14


  DELIMITER //

CREATE OR REPLACE FUNCTION ej14ejemp3(n1 int,n2 int,n3 int,n4 int) 

    RETURNS int (11)

  BEGIN

    DECLARE m int;
  CREATE OR REPLACE TEMPORARY TABLE temp (
    id int AUTO_INCREMENT,
    valor int,
    PRIMARY KEY (id)
  );

  INSERT INTO temp(valor) VALUES (n1,n2,n3,n4);
  SELECT valor INTO m FROM temp t
  GROUP BY t.valor
  ORDER BY COUNT(*) DESC LIMIT 1;
  RETURN m;

 END //
DELIMITER;


-- ejercicio 15



  DELIMITER //

 CREATE OR REPLACE PROCEDURE ej15ejemp3 (id1 int,id2 int)

  BEGIN
    UPDATE alumnos a set

      a.min=minimo (a.nota1,a.nota2,a.nota3,a.nota4),
      a.max=maximo (a.nota1,a.nota2,a.nota3,a.nota4),
      a.media=a.media (a.nota1,a.nota2,a.nota3,a.nota4),
      a.moda=moda (a.nota1,a.nota2,a.nota3,a.nota4)
   BETWEEN id1 AND id2;
  
    UPDATE grupos g



      SET media=(SELECT AVG(media) med1 FROM alumnos a  WHERE id BETWEEN id1 AND id2 AND grupo=1) WHERE g.id=1;

  UPDATE grupos g


        SET media=(SELECT AVG(media) med1 FROM alumnos a  WHERE id BETWEEN id1 AND id2 AND grupo=2) WHERE g.id=2;

    END //
DELIMITER

   
-- ejercicio 16 

   DELIMITER //
CREATE OR REPLACE PROCEDURE ej16ejemp3(id1 int)

  BEGIN

  UPDATE conversion c
    
   
 SET c.m=c.cm*100, c.km=c.cm/100000,c.pulgadas=c.cm/2.54
    WHERE cm IS NOT FALSE AND id>id1;

  UPDATE conversion c

  SET cm=m*100,km=m/1000,pulgadas=m*39.37
      WHERE m IS NOT NULL AND id>id1;


  UPDATE conversion c

   SET cm=km*100000,m=km*1000,pulgadas=km*39370.079
      WHERE km IS NOT NULL AND id>id1;

  UPDATE conversion c

  SET m=pulgadas/39.37,cm=pulgadas*2.54,km=pulgadas/39370.079
      WHERE pulgadas IS NOT NULL AND id>id1;





  END //


DELIMITER;

CALL ej16ejemp3(1);

SELECT * FROM conversion c;










   






  





  
  





