﻿USE ejemplo5programacion;

 -- ejercicio 1

DELIMITER //
CREATE OR REPLACE PROCEDURE ej1ejemp5(vpais varchar(50))
  BEGIN

SELECT * FROM cliente c WHERE c.pais=vpais;
 
 END //

DELIMITER;

CALL ej1ejemp5('usa');

-- ejercicio 2

DELIMITER //

CREATE OR REPLACE FUNCTION ej2ejemp5(vpais varchar(50))
  
  RETURNS int
  BEGIN
 DECLARE num int DEFAULT 0;

   SELECT COUNT(*) INTO num FROM cliente c WHERE vpais=c.pais;
  
  RETURN num;
  END //
DELIMITER;


-- ejercicio 4


  DELIMITER //

CREATE OR REPLACE FUNCTION ej4emp5 (num int)
RETURNS int
  BEGIN
 DECLARE numtotal int DEFAULT 0;
  SELECT COUNT(*) INTO numtotal FROM producto p WHERE p.cantidad_en_stock>num;
  

  RETURN numtotal;
  END //

DELIMITER;


DELIMITER //

CREATE OR REPLACE PROCEDURE ej6ejemp5(fp varchar(20))

  BEGIN
 
 
 SELECT max(total) FROM pago WHERE forma_pago=fp;

 END //

  DELIMITER;

CALL ej6ejemp5('transferencia');


DELIMITER //

CREATE OR REPLACE PROCEDURE ej7ejemp5(fp varchar(40))

  BEGIN

      SELECT MAX(total)valormaximo,MIN(total)valorminimo,AVG(total)valormedio,SUM(p.total)sumavalor,COUNT(*)cuentavalor FROM pago p WHERE p.forma_pago=fp;

  END //

DELIMITER;

CALL ej7ejemp5('transferencia');


DELIMITER //

CREATE OR REPLACE FUNCTION ej8ejemp5(nombre varchar(20))

  RETURNS varchar,

BEGIN

   DECLARE valormedio float;


  SELECT AVG(p.precio_venta)  INTO valormedio  FROM producto WHERE proveedor=nombre;

  RETURN valormedio;
  END //

  DELIMITER;








